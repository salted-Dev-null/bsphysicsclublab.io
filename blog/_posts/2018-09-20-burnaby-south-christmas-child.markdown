---
layout: post
time: 2018-09-20 22:07:00
title: "Students: Don't let Burnaby South associate with Operation Christmas Child ever again."
description: "Operation Christmas Child is terrible and Burnaby South should never take part in it again. This article is a warning to all students: RESEARCH!"
author: Madmin
---

## Preamble

This is not an attack on Interact Club. In 2017, we at <span class="spasm">SPaSM Club</span> brought up the issues described in this post to Interact Club. Everything was civil and the admins of Interact Club agreed to drop the *Operation Christmas Child* event forever. 

However, due to Burnaby South's legacy with this event, we don't feel that the issue is completely closed. As generations of admins graduate, leaving new admins in charge, information can disappear. We want to prevent Burnaby South from ever associating with *Operation Christmas Child* and its parent organization Samaritan's Purse, whether through Interact or any other club.

Thus, this post exists to immortalize a warning to all Burnaby South students as well as the general public. Always remember to undergo due research on everything you support, whether as a club or a student.

## What's the big deal?

Up until this year, it used to be a yearly Burnaby South tradition to participate in *Operation Christmas Child*, a Christmas event where students donate gifts to children in need. Many Burnaby South students would fill up a shoebox full of goodies for a child in a developing country in the name of holiday spirit. The event is popular in Burnaby South because it sounds like a cheap and hassle-free way of helping out the global community, but behind these events is a nefarious organization upholding abhorrent values and entangled in financial corruption.

### Religion

*Operation Christmas Child* is headed by not-for-profit Samaritan’s Purse, a Christian organization. Although the organization does have some aims to help children, their primary concern is actually spreading their religious beliefs. It is to the point that [government officials have become concerned that the organization is not using governments funds appropriately](http://www.nytimes.com/2001/03/05/world/us-aids-conversion-minded-quake-relief-in-el-salvador.html). The director has even said of his organization, “We are first a Christian organization and second an aid organization.” 

On the [Operation Christmas Child website](https://www.samaritanspurse.org/operation-christmas-child/the-journey-of-a-shoebox/), the operation describes itself as follows:

{% include image.html name="journey.png" alt="The Amazing Journey of a Shoebox Gift Begins with You and Results in Evangelism, Discipleship, and Multiplication." %}

When the shoeboxes are distributed, “they also offer The Greatest Gift, a Gospel story booklet Samaritan’s Purse developed for shoebox recipients.” The website describes how churches leverage the shoebox distributions to convert children, especially "in orphanages and other at-risk areas".

{% include image.html name="outreach.png" alt="" %}

Regardless of your personal religious beliefs, the organization is clearly taking advantage of vulnerable children in developing countries. The end goal illustrated on the website isn't children happy with their new gifts, it's children adopting a new religion and sharing it with others, described as "discipleship and multiplication". 

{% include image.html name="discipleship.png" alt="" %}

I recently received an advertisement for *Operation Christmas Child* in the mail. It includes a description of a 12-year-old girl living without electricity. Rather than focus on any improvement to her living conditions, the advertisement emphasizes her conversion to Christianity:

{% include image.html name="malawi.jpg" alt="" %}

In fact, they completely admit that sharing religion is "more important" than safe water and improved farming: 

{% include image.html name="why.jpg" alt="" %}

*Operation Christmas Child* treats the shoebox gifts as secondary to indoctrination, and has no qualms taking advantage of desperate people to do it. By supporting *Operation Christmas Child*, Burnaby South gives a helping hand to this manipulative practice. 

However, none of this has been told to the people at Burnaby South when Interact members advertised the shoebox program in the past. It was not written on the posters, nor mentioned in the classroom presentations. I have never participated in the event, but I'm told that people who do receive a pamphlet with the religious aspect of the organization explained, putting the onus on the students who willingly choose to participate even after reading the pamphlet. But many students in Burnaby South probably trusted their friends in Interact, and assumed that *Operation Christmas Child* was already thoroughly researched and vetted. 

It *is* wrong for students to blindly trust anything and forgo research, but it is also wrong to support as a school an organization whose primary aim is to spread religious beliefs. The actions of Burnaby South's clubs represent the school, and Burnaby South is a public school. We are religion-agnostic. Our students celebrate many faiths. It is inappropriate to show favouritism to *any* particular religion, especially by financially supporting it.

### Corruption and Controversy

Besides the religious element, there is controversy surrounding Samaritan’s Purse, the organization behind *Operation Christmas Child*, primarily involving its president, Franklin Graham. He has been accused multiple times of receiving an unusually large income from his two not-for-profit organizations. In 2009, he decided to give up his salary from the other organization, but it was revealed in 2015 that he had begun to receive it again. [This 2015 article](http://www.charlotteobserver.com/living/religion/article30505932.html) says he earned $669,000 in the year 2008 and $258,667 in the year 2014. 

[He is also very conservative in his views.](http://www.newsobserver.com/news/local/education/article180670531.html) He is against thorough sex-ed in schools and said that it [“is all part of an agenda to pervert the minds and steal the innocence of children—it’s wrong, and it’s evil.”](https://www.facebook.com/FranklinGraham/posts/1693240194065488)

{% include image.html name="sexed.png" alt="" %}

He is against LGBTQ representation in the media, [describing the LGBT lifestyle as “destructive” and urging parents to boycott Disney Channel due to a new program with a “gay storyline.”](https://www.facebook.com/FranklinGraham/posts/1702126869843487)

{% include image.html name="disney.png" alt="" %}

There’s a lot more racist, sexist, and homophobic material to be found on his official Facebook page.

This man does not represent Burnaby South’s values. We have a Gay-Straight Alliance. Decorating our classroom doors and windows are several pride- and acceptance-related posters. 

Participating in *Operation Christmas Child* requires a mandatory donation of at least $10. By now, we know that this money is going straight towards converting people in vulnerable positions and directly to Graham's pockets. None of these are things Burnaby South should support. 

## A better alternative?

Let's not lose our holiday tradition, but better it instead. We got into this situation because the children we tried to help were so geographically far away that helping them meant going through a third party, which ended up being the terrible *Operation Christmas Child*. No matter what, there is necessarily a level of uncertainty in how much we know about what we are doing for people so far away. 

It would be far more effective to help out locally. The shoebox operation is a nice idea, but I suggest an alternative. We can instead fill up shoeboxes and give them to the disadvantaged kids around Burnaby. There are plenty of kids in nearby elementary schools in need of some holiday magic every year. We don't always need to look towards developing countries to feel good about ourselves and make a difference. 

We hope we’ve been able to convince you to turn away from *Operation Christmas Child* and pursue an alternative path for your club's Christmas event. <span class="spasm">SPaSM Club</span> would be willing to help. 
