const startup = function() {

	const rgb2hex = function(rgb) {
	    if (/^#[0-9A-F]{6}$/i.test(rgb)) return rgb;
	
	    rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
	    const hex = function(x) {
	        return ("0" + parseInt(x).toString(16)).slice(-2);
	    }
	    return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
	}

	const update_strokes = function(event) {
		if (strokes) {
			for (let i = 0, n = strokes.length; i < n; i++) {
				strokes[i].style.stroke = event.target.value;
			}
		}
	}	

	const update_dark = function(event) {
	
		let color = tinycolor(event.target.value);
		color.darken(5);
		color.saturate(20);
	
		if (dark_stop) {
			for (let i = 0, n = dark_stop.length; i < n; i++) {
				dark_stop[i].style.stopColor = event.target.value;
			}
		}
	
		if (buttlines) {
			for (let i = 0, n = buttlines.length; i < n; i++) {
				buttlines[i].style.stroke = color.toString();
				}
		}

	}

	const update_light = function(event) {
	
		if (light_fills) {
			for (let i = 0, n = light_fills.length; i < n; i++) {
				light_fills[i].style.fill = event.target.value;
			}
	
		}
	
		if (light_stop) {
			for (let i = 0, n = light_stop.length; i < n; i++) {
				light_stop[i].style.stopColor = event.target.value;
			}
		}
	
	}
	
	const gen_svg = function() {
	
	    let svg = document.getElementById("klein");
	    let source = new XMLSerializer().serializeToString(svg);

	    //add name spaces.
	    if(!source.match(/^<svg[^>]+xmlns="http\:\/\/www\.w3\.org\/2000\/svg"/)){
	        source = source.replace(/^<svg/, '<svg xmlns="http://www.w3.org/2000/svg"');
	    }
	    if(!source.match(/^<svg[^>]+"http\:\/\/www\.w3\.org\/1999\/xlink"/)){
	        source = source.replace(/^<svg/, '<svg xmlns:xlink="http://www.w3.org/1999/xlink"');
	    }
		
    	//add xml declaration
    	source = '<?xml version="1.0" standalone="no"?>\r\n' + source;
	
	    let url = "data:image/svg+xml;charset=utf-8,"+encodeURIComponent(source);
	
		triggerDownload(url, "klein.svg");

	}
	
	const gen_png = function() {
	
	    let svg = document.getElementById("klein");
	    let source = new XMLSerializer().serializeToString(svg);
	
	    let canvas = document.createElement("canvas");
	    canvas.width = svg.width.baseVal.value;
	    canvas.height = svg.height.baseVal.value;
	
	    let ctx = canvas.getContext("2d");
	
	    let img = document.createElement("img");
	    img.setAttribute("src", "data:image/svg+xml;base64," + btoa(source));
		console.log(img);
	
	    let url = "";
	
	    img.onload = function() {
	        ctx.drawImage(img, 0, 0);
	    
	        url = canvas.toDataURL("image/png");
			console.log(url);
	        triggerDownload(url, "klein.png");
	    };
	
	}
	
	const triggerDownload = function(imgURI, fileName) {
	    let evt = new MouseEvent("click", {
	        view: window,
	        bubbles: false,
	        cancelable: true
	    });
	    let a = document.createElement("a");
	    a.setAttribute("download", fileName);
	    a.setAttribute("href", imgURI);
	    a.setAttribute("target", '_blank');
	    a.dispatchEvent(evt);
	}
	
	
	let controls = document.getElementsByClassName("controls");
	for (let i = 0, n = controls.length; i < n; i++) {
		controls[i].style.display = "block";
	}	

	let button_svg = document.getElementById("button-svg");
	let button_png = document.getElementById("button-png");

	button_svg.addEventListener("click", gen_svg, false);
	button_png.addEventListener("click", gen_png, false);

	let bottle = document.getElementById("klein");
	let strokes = document.getElementsByClassName("stroke");
	let light_fills = document.getElementsByClassName("light-fill");
	let buttlines = document.getElementsByClassName("buttline");
	let light_stop = document.getElementsByClassName("light-stop");
	let dark_stop = document.getElementsByClassName("dark-stop");

	let light_select = document.getElementById("light-select");
	let dark_select = document.getElementById("dark-select");
	let stroke_select = document.getElementById("stroke-select");
	
	stroke_select.addEventListener("input", update_strokes, false);
	stroke_select.select();

	dark_select.addEventListener("input", update_dark, false);
	dark_select.select();
	
	light_select.addEventListener("input", update_light, false);
	light_select.select();
	
}

window.addEventListener("load", startup, false);
